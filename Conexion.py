import psycopg2 as pg

def get_db_conexion():
    try:
        conexion = pg.connect(host='localhost',
                              database='kemok',
                              user='postgres',
                              password='123456')

        return conexion
    except pg.DatabaseError as error:
        print("Error: {0}".format(error))

def cerrar_db_conexion(conexion):
    try:
        conexion.close()
    except pg.DatabaseError as error:
        print("Error: {0}".format(error))
