import Conexion as cn
import psycopg2 as pg

def load_companies():
    try:
        connection = cn.get_db_conexion()
        cursor = connection.cursor()
        file = open('C:\\Users\\Usuario\\Desktop\\kemok_prueba\\kemok_prueba\\plain_data\\companies.csv', "r")
        cursor.execute("Truncate {} Cascade;".format('companies'))
        cursor.copy_expert("copy {} from STDIN CSV HEADER QUOTE '\"'".format('companies'), file)
        cursor.execute("commit;")
        cn.cerrar_db_conexion(connection)
        print("Companies load succesfully")

    except Exception as error:
        print("Error: {}".format(str(error)))

def load_opportunities():
    try:
        connection = cn.get_db_conexion()
        cursor = connection.cursor()
        file = open('C:\\Users\\Usuario\\Desktop\\kemok_prueba\\kemok_prueba\\plain_data\\opportunity.csv', "r")
        cursor.execute("Truncate {} Cascade;".format('opportunity'))
        cursor.copy_expert("copy {} from STDIN CSV HEADER QUOTE '\"'".format('opportunity'), file)
        cursor.execute("commit;")
        cn.cerrar_db_conexion(connection)
        print("Opportunity load succesfully")

    except Exception as error:
        print("Error: {}".format(str(error)))

def load_products():
    try:
        connection = cn.get_db_conexion()
        cursor = connection.cursor()
        file = open('C:\\Users\\Usuario\\Desktop\\kemok_prueba\\kemok_prueba\\plain_data\\products.csv', "r")
        cursor.execute("Truncate {} Cascade;".format('products'))
        cursor.copy_expert("copy {} from STDIN CSV HEADER QUOTE '\"'".format('products'), file)
        cursor.execute("commit;")
        cn.cerrar_db_conexion(connection)
        print("Products load succesfully")

    except Exception as error:
        print("Error: {}".format(str(error)))

def load_rfq():
    try:
        connection = cn.get_db_conexion()
        cursor = connection.cursor()
        file = open('C:\\Users\\Usuario\\Desktop\\kemok_prueba\\kemok_prueba\\plain_data\\rfq.csv', "r")
        cursor.execute("Truncate {} Cascade;".format('rfq'))
        cursor.copy_expert("copy {} from STDIN CSV HEADER QUOTE '\"'".format('rfq'), file)
        cursor.execute("commit;")
        cn.cerrar_db_conexion(connection)
        print("Products Price load succesfully")

    except Exception as error:
        print("Error: {}".format(str(error)))

def load_sales():
    try:
        connection = cn.get_db_conexion()
        cursor = connection.cursor()
        file = open('C:\\Users\\Usuario\\Desktop\\kemok_prueba\\kemok_prueba\\plain_data\\sales.csv', "r")
        cursor.execute("Truncate {} Cascade;".format('sales'))
        cursor.copy_expert("copy {} from STDIN CSV HEADER QUOTE '\"'".format('sales'), file)
        cursor.execute("commit;")
        cn.cerrar_db_conexion(connection)
        print("Sales load succesfully")

    except Exception as error:
        print("Error: {}".format(str(error)))

def load_product_list():
    try:
        connection = cn.get_db_conexion()
        cursor = connection.cursor()
        file = open('C:\\Users\\Usuario\\Desktop\\kemok_prueba\\kemok_prueba\\plain_data\\product_list.csv', "r")
        cursor.execute("Truncate {} Cascade;".format('product_list'))
        cursor.copy_expert("copy {} from STDIN CSV HEADER QUOTE '\"'".format('product_list'), file)
        cursor.execute("commit;")
        cn.cerrar_db_conexion(connection)
        print("Product List load succesfully")

    except Exception as error:
        print("Error: {}".format(str(error)))

load_companies()
load_opportunities()
load_products()
load_rfq()
load_sales()
load_product_list()

