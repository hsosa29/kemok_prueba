Technical assessment - Kemok

1. Create the corresponding tables in postgresql (script.sql)
2. Script for database connection from python (Conexion.py)
3. Script for the load data from csv or xlsx files to postgresql (Load_Data.py)
4. Data Cleaning in database (data_cleaning.sql)
5. Create a materialized view at name hellolabs (materialized_view_hellolabs.sql)
6. Create queries that view must be (sql_queries.sql)
7. Make the prototype dashboard using an open source software to make dashboard (Metabase), using the data from the postgresql database (Dashboard.jpg)
8. Finally resolve the three questions (Questions.docx)

Thanks,

Hugo Sosa
