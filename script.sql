-- Table: public.companies

-- DROP TABLE public.companies;

CREATE TABLE public.companies
(
    nit text COLLATE pg_catalog."default",
    name text COLLATE pg_catalog."default"
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.companies
    OWNER to postgres;
	
	
-- Table: public.opportunity

-- DROP TABLE public.opportunity;

CREATE TABLE public.opportunity
(
    id_opportunity integer NOT NULL,
    description text COLLATE pg_catalog."default",
    date text COLLATE pg_catalog."default",
    status text COLLATE pg_catalog."default",
    CONSTRAINT opportunity_pkey PRIMARY KEY (id_opportunity)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.opportunity
    OWNER to postgres;
	

-- Table: public.products

-- DROP TABLE public.products;

CREATE TABLE public.products
(
    id_product integer NOT NULL,
    id_opportunity integer,
    name text COLLATE pg_catalog."default",
    demand integer,
    CONSTRAINT products_pkey PRIMARY KEY (id_product)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.products
    OWNER to postgres;


-- Table: public.rfq

-- DROP TABLE public.rfq;

CREATE TABLE public.rfq
(
    id_product integer,
    nit text COLLATE pg_catalog."default",
    unitary_price numeric(10,3)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.rfq
    OWNER to postgres;
	
-- Table: public.sales

-- DROP TABLE public.sales;

CREATE TABLE public.sales
(
    nit text COLLATE pg_catalog."default",
    amount numeric(10,2),
    id_oportunity integer
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.sales
    OWNER to postgres;

-- Table: public.product_list

-- DROP TABLE public.product_list;

CREATE TABLE public.product_list
(
    products_of_interest text COLLATE pg_catalog."default"
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.product_list
    OWNER to postgres;