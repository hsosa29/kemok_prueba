/*a.	The view must be summarized by month.  */
SELECT nit_company, name_company, opportunity_descrition, opportunity_status, date_part('month' ,TO_DATE(opportunity_date, 'mm/dd/yyyy')), SUM(sales_amount) AS month 
FROM hellolabs
GROUP BY
nit_company, name_company, opportunity_descrition, opportunity_status, date_part('month' ,TO_DATE(opportunity_date, 'mm/dd/yyyy'))

/*b.	The view must include minimum price point of sale if available.*/
SELECT nit_company, name_company, MIN(unitary_price) AS month 
FROM hellolabs
GROUP BY
nit_company, name_company
ORDER BY name_company

/*c.	The view must include the sales of each company in the market.*/
SELECT nit_company, name_company, SUM(sales_amount) AS sales 
FROM hellolabs
GROUP BY
nit_company, name_company
ORDER BY name_company

/*d.	The view must have a column for the cumulate sale by month. (All months must be accounted for)*/
SELECT date_part('year' ,TO_DATE(opportunity_date, 'mm/dd/yyyy')) AS year,date_part('month' ,TO_DATE(opportunity_date, 'mm/dd/yyyy')), SUM(sales_amount) AS month 
FROM hellolabs
GROUP BY
date_part('year' ,TO_DATE(opportunity_date, 'mm/dd/yyyy')), date_part('month' ,TO_DATE(opportunity_date, 'mm/dd/yyyy'))
ORDER BY YEAR, MONTH

/*e.	The view must have a column for the sales of the product a year before the date.*/
SELECT product_name, date_part('year' ,TO_DATE(opportunity_date, 'mm/dd/yyyy')) AS year, date_part('month' ,TO_DATE(opportunity_date, 'mm/dd/yyyy')) AS month, SUM(sales_amount) AS sales 
FROM hellolabs
GROUP BY
product_name ,date_part('year' ,TO_DATE(opportunity_date, 'mm/dd/yyyy')), date_part('month' ,TO_DATE(opportunity_date, 'mm/dd/yyyy'))
ORDER BY product_name, YEAR, MONTH

/*f.	The view must have a column for the cumulate sale of the product a year before the date.*/
SELECT product_name, date_part('year' ,TO_DATE(opportunity_date, 'mm/dd/yyyy')) AS year, SUM(sales_amount) AS sales 
FROM hellolabs
GROUP BY
product_name ,date_part('year' ,TO_DATE(opportunity_date, 'mm/dd/yyyy'))
ORDER BY product_name, YEAR



